import axios from '../../axios';
import router from '../../router';

export default {
    SIGNIN({commit}, payload) {
        const data = {
            email: payload.email,
            password: payload.password,
            remember_me: payload.checkbox_remember_me,
        };
        axios.post('login', data).then(response => {
            commit('SET_TOKEN', [response.data.meta.token, response.data.data]);

            router.push('/')

            payload.$vs.notify({
                position:'top-center',
                title: 'Успешный вход',
                text: 'С возвращением!',
                iconPack: 'feather',
                icon: 'icon-alert-circle',
                color: 'success'
            })

        }).catch(() => {
            payload.loading = false
            payload.$vs.notify({
                time: 2500,
                title: 'Error',
                text: 'Ошибка авторизации',
                iconPack: 'feather',
                icon: 'icon-alert-circle',
                position: 'top-center',
                color: 'danger'
            });
        })
    },

    GET_DATA({commit}) {
        let profile = JSON.parse(localStorage.getItem("user-profile"));

        const http = axios.create({
            baseURL: apiUrl,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        });
        http.interceptors.request.use(config => {
            config.headers.Authorization = 'Bearer ' + localStorage.getItem("user-token");
            return config
        });

        http.get('users/' + profile.id).then(response => {
            let getNewData = JSON.stringify(response.data.user);
            commit('UPDATE_USER_DATA', getNewData);
        }).catch(error => {
            console.log(error);
        })
    },

    GET_AUTH(state) {
        if (state.token) {
            state.isAuthenticated = true
        }
    }
}
