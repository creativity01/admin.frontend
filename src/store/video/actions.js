export default{
    GET_VIDEOS({commit}, payload){
        payload.$http.get('video').then(response => {
            commit('SET_VIDEOS', response.data.videos)
        })
    },
    CREATE_VIDEO({commit}, payload){
        let formData = new FormData()
        formData.append('title', payload.title)
        formData.append('img', payload.dataImg)
        formData.append('category_id', payload.category)
        formData.append('description', payload.description)
        payload.$http.post('/video', formData,
        {
          headers: {
              'Content-Type': 'multipart/form-data'
          }
        }).then(response => {
            commit('SET_VIDEO', response.data.video)
        })
    },
    UPDATE_VIDEO({commit}, payload){
        let formData = new FormData()
        formData.append('title', payload.title)
        formData.append('img', payload.dataImg)
        formData.append('category_id', payload.category)
        formData.append('description', payload.description)
        payload.$http.post(`video/${payload.data.id}`, formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
          }).then(response => {
            commit('UPDATE_VIDEO', response.data.video)
        })
    },
    DELETE_VIDEO({commit}, payload){
        payload.$http.delete(`video/${payload.deletedId}`).then(() => {
            commit('DELETE_VIDEO', payload.deletedId)
        })
    },
    TOGGLE_PUBLISH({commit}, payload){
        payload.$http.put(`video/toggle_publish/${payload.publishedId}`,{
            videoId:payload.publishedId
        }).then(response => {
            commit('TOGGLE_PUBLISH', response.data.video)
            payload.publishedId = null
            payload.$vs.notify({
                title: 'Ураа...',
                text: "Опубликовано",
                color: 'success',
                iconPack: 'feather',
                position: 'top-center',
                icon: 'icon-alert-circle'
            })
        }).catch(err => {            
            payload.$vs.notify({
                title: 'Уппс!',
                text: err.response.data.errors.videoId,
                color:'warning',
                position: 'top-center',
                icon:'error'
            })
        })
    }
    // DELETE_SELECTED_CATEGORY({commit}, payload){
    //     const obj = {selected: payload.selectedId}
    //     payload.$http.patch(`category/delete_selected`, obj).then(() => {
    //         commit('DELETE_SELECTED_CATEGORY', payload.selectedId)
    //     })
    // }
}