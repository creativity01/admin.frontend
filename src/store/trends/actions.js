export default{
    GET_CATEGORY({commit}, payload){
        payload.$http.get('category').then(response => {
            commit('SET_CATEGORIES', response.data.categories)
        })
    },
    CREATE_CATEGORY({commit}, payload){
        let formData = new FormData()
        formData.append('title', payload.dataName)
        formData.append('img', payload.dataImg)
        payload.$http.post('category', formData).then(response => {
            commit('SET_CATEGORY', response.data.category)
        })
    },
    UPDATE_CATEGORY({commit}, payload){
        let formData = new FormData()
        formData.append('title', payload.dataName)
        formData.append('img', payload.dataImg)
        payload.$http.post(`category/${payload.data.id}`, formData).then(response => {
            commit('UPDATE_CATEGORY', response.data.category)
        })
    },
    DELETE_CATEGORY({commit}, payload){
        payload.$http.delete(`category/${payload.deletedId}`).then(() => {
            commit('DELETE_CATEGORY', payload.deletedId)
        })
    },
    DELETE_SELECTED_CATEGORY({commit}, payload){
        const obj = {selected: payload.selectedId}
        payload.$http.patch(`category/delete_selected`, obj).then(() => {
            commit('DELETE_SELECTED_CATEGORY', payload.selectedId)
        })
    }
}