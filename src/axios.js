// axios
import axios from 'axios'

const baseURL = "http://creativity.loc/api/"

export default axios.create({
  baseURL: baseURL,
  headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
  }
});
