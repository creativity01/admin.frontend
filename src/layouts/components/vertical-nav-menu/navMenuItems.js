export default [
  {
    url: "/",
    name: "Главная",
    slug: "home",
    icon: "HomeIcon",
  },
  {
    url:null,
    name: "Тренды",
    slug: "category",
    icon: "PackageIcon",
    submenu:[
      {
        url: "/category",
        name: "Категории",
        slug: "category",
      },
      // {
      //   url: "/sub-category",
      //   name: "Под категории",
      //   slug: "sub_category",
      // }
    ]
  },
  {
    url: "/video",
    name: "video",
    slug: "video",
    icon: "ArchiveIcon",
  },
]
